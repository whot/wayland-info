wayland-info
============

Because `weston-info` is a generally useful tool, even outside of weston,
and gives interesting information on any Wayland compositor, this is basically
a standalone version of `weston-info` as found in
[weston](https://gitlab.freedesktop.org/wayland/weston/) repository.

Building
========

Just like `weston` itself, `wayland-info` is built using
[Meson](https://mesonbuild.com/) and depends
on [Wayland](https://gitlab.freedesktop.org/wayland/wayland) and
[wayland-protocols](https://cgit.freedesktop.org/wayland/wayland-protocols).

	$ git clone https://gitlab.freedesktop.org/ofourdan/wayland-info.git
	$ cd wayland-info
	$ meson build/ --prefix=...
	$ ninja -C build/ install
	$ cd ..

Running
=======

`wayland-info` does not accepts any command line option.

	$ wayland-info

Credit
======

The code base of `wayland-info` is identical to `weston-info` and therefore
all credits for `wayland-info` go to the author and contributors of `weston-info`.

