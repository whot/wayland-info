.TH WAYLAND-INFO 1 "2020-07-08" "@version@"
.SH NAME
wayland-info \- display information utility for Wayland.
.SH SYNOPSIS
.B wayland-info
.
.\" ***************************************************************
.SH DESCRIPTION

.B wayland-info
is a utility for displaying information about the Wayland protocols supported
by a Wayland compositor.

It is used to check which Wayland protocols and versions are advertised
by the Wayland compositor.

.B wayland-info
also provides additional information for a subset of Wayland protocols it
knows about, namely Linux DMABUF, presentation time, tablet and XDG output
protocols.
.
.\" ***************************************************************
.SH OPTIONS
.
.B wayland-info
does not accept any command line option.
.
.\" ***************************************************************
.SH ENVIRONMENT
.
.TP
.B WAYLAND_DISPLAY
The name of the display (socket) of an already running Wayland server, without
the path. The directory path is always taken from
.BR XDG_RUNTIME_DIR .
If
.B WAYLAND_DISPLAY
is not set, the socket name is "wayland-0".
.
.\" ***************************************************************
.SH "SEE ALSO"
.BR weston (1)
